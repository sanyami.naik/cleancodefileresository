package com.CleanCoding.Clean.Coding.repository;

import com.CleanCoding.Clean.Coding.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Long> {
}
