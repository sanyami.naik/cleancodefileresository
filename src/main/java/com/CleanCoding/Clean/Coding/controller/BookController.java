package com.CleanCoding.Clean.Coding.controller;


import com.CleanCoding.Clean.Coding.entity.Book;
import com.CleanCoding.Clean.Coding.service.BookService;
import com.CleanCoding.Clean.Coding.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    BookService bookService;

    @Autowired
    LibraryService libraryService;

    @PostMapping
    public ResponseEntity<Book> addBook(@RequestBody Book book)
    {
        return new ResponseEntity<>(libraryService.addBook(book),HttpStatus.CREATED);
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<Book> retrieveBookDetails(@PathVariable long bookId)
    {
        return new ResponseEntity<>(bookService.getBookDetails(bookId), HttpStatus.OK);
    }

    @PutMapping("/{bookId}")
    public ResponseEntity<Book> updateBookDetails(@PathVariable long bookId, @RequestBody Book book)
    {
        return new ResponseEntity<>(bookService.updateBookDetails(bookId,book), HttpStatus.OK);
    }

    @GetMapping("/searchBook/{bookTitle}")
    public ResponseEntity<Book> searchByBookTitle(@PathVariable String bookTitle)
    {
        return new ResponseEntity<>(libraryService.searchByBookTitle(bookTitle),HttpStatus.OK);
    }
}
