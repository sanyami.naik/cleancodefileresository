package com.CleanCoding.Clean.Coding.entity;


import javax.persistence.*;

@Entity
public class Book {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     private long bookId;
     private String bookTitle;
     private String authorName;
     private int publicationYear;

    public Book() {
    }

    public Book(long bookId, String bookTitle, String authorName, int publicationYear) {
        this.bookId = bookId;
        this.bookTitle = bookTitle;
        this.authorName = authorName;
        this.publicationYear = publicationYear;
    }

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }
}
