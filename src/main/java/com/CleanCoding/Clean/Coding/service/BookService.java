package com.CleanCoding.Clean.Coding.service;

import com.CleanCoding.Clean.Coding.entity.Book;

import java.util.Optional;

public interface BookService {

    Book getBookDetails(long bookId);

    Book updateBookDetails(long bookId, Book book);
}
