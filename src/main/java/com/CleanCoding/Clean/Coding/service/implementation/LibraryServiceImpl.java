package com.CleanCoding.Clean.Coding.service.implementation;

import com.CleanCoding.Clean.Coding.entity.Book;
import com.CleanCoding.Clean.Coding.repository.BookRepository;
import com.CleanCoding.Clean.Coding.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class LibraryServiceImpl implements LibraryService {

    @Autowired
    BookRepository bookRepository;

    @Override
    public Book addBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public Book searchByBookTitle(String bookTitle) {
        List<Book> bookList=bookRepository.findAll();
        Book searchedBook= (Book) bookList.stream().filter(bookNeeded->bookNeeded.getBookTitle().equals(bookTitle));
        return searchedBook;
    }


}
