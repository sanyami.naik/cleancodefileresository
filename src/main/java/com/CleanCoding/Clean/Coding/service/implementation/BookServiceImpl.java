package com.CleanCoding.Clean.Coding.service.implementation;

import com.CleanCoding.Clean.Coding.entity.Book;
import com.CleanCoding.Clean.Coding.repository.BookRepository;
import com.CleanCoding.Clean.Coding.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Override
    public Book getBookDetails(long bookId) {
        Optional<Book> book=bookRepository.findById(bookId);
        if(book.isPresent())
            return book.get();
        else
            return null;
    }

    @Override
    public Book updateBookDetails(long bookId, Book book) {
        Optional<Book> optionalBook=bookRepository.findById(bookId);
        if(optionalBook.isPresent())
        {
            Book bookToBeUpdated=optionalBook.get();
            bookToBeUpdated.setBookTitle(book.getBookTitle());
            bookToBeUpdated.setAuthorName(book.getAuthorName());
            bookToBeUpdated.setPublicationYear(book.getPublicationYear());
            return book;
        }
        else
            return null;
    }
}
