package com.CleanCoding.Clean.Coding.service;

import com.CleanCoding.Clean.Coding.entity.Book;

public interface LibraryService {

    Book addBook(Book book);

    Book searchByBookTitle(String bookTitle);
}
